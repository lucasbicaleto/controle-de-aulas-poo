<?php

include_once 'db.php';

Class Professor Extends Banco {

	public function lista($id = "", $tipo = '') {
		mysqli_set_charset($this->db_conexao, 'utf8');

		$professors = [];
		$whereId    = (!empty($id))   ? "WHERE professor = $id" : "";
		$whereTipo  = (!empty($tipo)) ? "WHERE p.tipo = '$tipo'" : "";
		$sqlBusca   = "SELECT p.professor, p.nome, d.disciplina, d.descricao, p.tipo, d.codigo,f.data
					 FROM professors p
					 JOIN disciplinas d ON d.disciplina = p.disciplina 
					 LEFT JOIN ferias f ON f.professor = p.professor
					 $whereId
					 $whereTipo";
		$resBusca = mysqli_query($this->db_conexao, $sqlBusca);

		if (mysqli_num_rows($resBusca) > 0) {

			for ($i = 0; $i < mysqli_num_rows($resBusca); $i++) {
				$object 	= mysqli_fetch_object($resBusca);

				$professors[] = [
					'id_professor'   => $object->professor,
					'nome'           => $object->nome,
					'tipo'           => $object->tipo,
					'disciplina'     => $object->disciplina,
					'disciplina_cod' => $object->codigo,
					'descricao'      => $object->descricao,
					'data_ferias'    => $object->data
				];
			}

			return json_encode([
				'professores' => $professors,
				'erro ' 	  => false,
				'mensagem'    => ''
			]);
			
			exit;
		}
	}
}