<?php

include_once 'db.php';

Class Disciplina Extends Banco {

	public function lista($id = "") {
		mysqli_set_charset($this->db_conexao, 'utf8');

		$disciplinas = [];
		$whereId     = (!empty($id)) ? "WHERE p.professor = $id" : "";
		$sqlBusca    = "SELECT disciplina,codigo,descricao FROM disciplinas $whereId";
		$resBusca    = mysqli_query($this->db_conexao, $sqlBusca);

		if (mysqli_num_rows($resBusca) > 0) {

			for ($i = 0; $i < mysqli_num_rows($resBusca); $i++) {
				$object 	= mysqli_fetch_object($resBusca);

				$disciplinas[] = [
					'id'         => $object->disciplina,
					'codigo'     => $object->codigo,
					'descricao'  => $object->descricao
				];
			}

			return json_encode([
				'disciplinas' => $disciplinas,
				'erro ' 	  => false,
				'mensagem'    => ''
			]);
			
			exit;
		}
	}

}