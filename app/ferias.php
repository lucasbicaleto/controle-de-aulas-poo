<?php

include_once 'db.php';

Class Ferias Extends Banco {

	public function lista($id = "") {
		mysqli_set_charset($this->db_conexao, 'utf8');

		$ferias     = [];
		$whereId    = (!empty($id)) ? "WHERE p.professor = $id" : "";
		$sqlBusca   = "SELECT f.ferias,f.data, p.nome, d.disciplina, d.descricao, p.professor, d.codigo
					 FROM ferias f
					 JOIN professors p ON p.professor = f.professor
					 JOIN disciplinas d ON d.disciplina = p.disciplina 
					 $whereId";
		$resBusca = mysqli_query($this->db_conexao, $sqlBusca);

		if (mysqli_num_rows($resBusca) > 0) {

			for ($i = 0; $i < mysqli_num_rows($resBusca); $i++) {
				$object 	= mysqli_fetch_object($resBusca);

				$ferias[] = [
					'id_ferias'      => $object->ferias,
					'dia'            => $object->data,
					'id_professor'   => $object->professor,
					'professor'      => $object->nome,
					'disciplina'     => $object->disciplina,
					'disciplina_cod' => $object->codigo,
					'descricao'      => $object->descricao
				];
			}

			return json_encode([
				'ferias'      => $ferias,
				'erro ' 	  => false,
				'mensagem'    => ''
			]);
			
			exit;
		}
	}
}