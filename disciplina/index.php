<?php

include_once '../app/disciplina.php';

$classDisciplinas = New Disciplina();
$listagem         = json_decode($classDisciplinas->lista(),true);

include_once '../dist/template/cabecalho.php';
?>            

<div class="container-fluid">
	 <div class="card mb-4 mt-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Disciplinas Cadastradas
        </div>
        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Código</th>
                            <th>Descrição</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if (count($listagem['disciplinas']) > 0) {

                                foreach ($listagem AS $chave => $disciplinaDados) {

                                	foreach ($disciplinaDados AS $row) {
				  		
	                                    echo "<tr> 
	                                            <td> ".$row['id']." </td>
	                                            <td> ".$row['codigo']." </td>
	                                            <td> ".$row['descricao']." </td>
	                                            <td> <i class='fas fa-edit fa-lg' style='color: #ffc107 !important;'></i> <i class='fas fa-trash fa-lg' style='color: red !important;'></i> </td>
	                                         </tr>";
						  			}
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>     

<?php include_once '../dist/template/rodape.php'; ?>
        