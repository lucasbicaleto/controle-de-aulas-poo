<?php

include_once '../app/ferias.php';

$classFerias = New Ferias();
$listagem    = json_decode($classFerias->lista(),true);

include_once '../dist/template/cabecalho.php';
?>            

<div class="container-fluid">
	 <div class="card mb-4 mt-4">
        <div class="card-header">
            <i class="fas fa-table mr-1"></i>
            Férias Cadastradas
        </div>
        
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Data</th>
                            <th>Professor</th>
                            <th>Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php 
                            if (count($listagem['ferias']) > 0) {

                                foreach ($listagem['ferias'] AS $chave => $feriasDados) {

                                    echo "<tr> 
                                            <td> ".$feriasDados['id_ferias']." </td>
                                            <td> ".$feriasDados['dia']." </td>
                                            <td> ".$feriasDados['professor']." - (".$feriasDados['descricao'].") </td>
                                           	<td> <i class='fas fa-edit fa-lg' style='color: #ffc107 !important;'></i> <i class='fas fa-trash fa-lg' style='color: red !important;'></i> </td>
                                         </tr>";
                                }
                            }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>    

<?php include_once '../dist/template/rodape.php'; ?>
        