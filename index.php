<?php

include_once 'app/disciplina.php';
include_once 'app/ferias.php';
include_once 'app/professor.php';
include_once 'app/aula.php';

$classDisciplinas = New Disciplina();
$classProfessores = New Professor();
$classFerias      = New Ferias();
$classAulas       = New Aula();

$disciplinas = json_decode($classDisciplinas->lista(),true);
$professores = json_decode($classProfessores->lista('', 'fixo'),true);
$substitutos = json_decode($classProfessores->lista('','substituto'),true);
$ferias      = json_decode($classFerias->lista(),true);
$aulas       = json_decode($classAulas->lista($professores,$substitutos,$disciplinas,$ferias),true);

/*echo "<pre>";
print_r($aulas);
echo "</pre>";
exit;*/

include_once './dist/template/cabecalho.php';
?>            

<div class="container-fluid">

	<!-- dados -->
	<div class="row mt-5">
        <div class="col-xl-3 col-md-6">
            <div class="card bg-primary text-white mb-4">
                <div class="card-body textCount"> <?= count($professores['professores']); ?> </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <span class="small text-white stretched-link text-center">PROFESSORES CADASTRADOS</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-warning text-white mb-4">
                <div class="card-body textCount"> <?= count($substitutos['professores']); ?> </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                   <span class="small text-white stretched-link text-center">SUBISTITUTOS CADASTRADOS</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-success text-white mb-4">
                <div class="card-body textCount"> <?= count($disciplinas['disciplinas']); ?> </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <span class="small text-white stretched-link text-center">DISCIPLINAS CADASTRADAS</span>
                </div>
            </div>
        </div>
        <div class="col-xl-3 col-md-6">
            <div class="card bg-danger text-white mb-4">
                <div class="card-body textCount"> <?= count($ferias['ferias']); ?> </div>
                <div class="card-footer d-flex align-items-center justify-content-between">
                    <span class="small text-white stretched-link text-center">FÉRIAS CADASTRADAS</span>
                </div>
            </div>
        </div>
    </div>

    <!-- aulas -->
		<header>
		<h4 class="display-4 mb-4 text-center">Controle de Aulas - Junho 2019</h4>
		 		<div class="row d-none d-sm-flex p-1 bg-dark text-white">
	      		<h5 class="col-sm p-1 text-center">Segunda</h5>
			    <h5 class="col-sm p-1 text-center">Terça</h5>
		      	<h5 class="col-sm p-1 text-center">Quarta</h5>
		      	<h5 class="col-sm p-1 text-center">Quinta</h5>
		      	<h5 class="col-sm p-1 text-center">Sexta</h5>
		      	<h5 class="col-sm p-1 text-center">Sábado</h5>
		      	<h5 class="col-sm p-1 text-center">Domingo</h5>
			</div>
		</header>
		<div class="row border border-right-0 border-bottom-0">

			<?php 
				/*	
				echo "<pre>";
				print_r($aulas);
				echo "</pre>";
				exit;
				*/
		?>
    	<?php 
    		foreach ($aulas['dia'] AS $dia => $dadosAulas) { 

    		$style = (isset($dadosAulas['fds']) && $dadosAulas['fds'] == true) ? "style='min-width: 140px !important; max-width: 160px !important;'" : "style='max-width: 160px !important;'";
		?>

    		<div class="day col-sm p-2 border border-left-0 border-top-0 d-none d-sm-inline-block bg-light text-muted"<?=$style?>>
    			<h5 class="row align-items-center">
		        	<span class="date col-1"> <?= $dia; ?> </span>
	      		</h5>
	      		
	      		<?php
					$grade = $dadosAulas['grade'];
					$bg    = ['info','warning','success','light'];

					if (count($grade) > 0) {

						foreach ($grade AS $aula) {

							switch ($aula['disciplina_cod']) {
								case 'his': $bg = 'new1';    break;
								case 'fis': $bg = 'new2';    break;
								case 'lit': $bg = 'info';    break;
								case 'geo': $bg = 'dark';    break;
								case 'ing': $bg = 'danger';  break;
								case 'mat': $bg = 'primary'; break;
								case 'por': $bg = 'success'; break;
								case 'esp': $bg = 'warning'; break;
								default:    $bg = 'success'; break;
							}

							echo '<span class="event d-block p-1 pl-2 pr-2 mb-1 rounded text-truncate small bg-'.$bg.' text-white" title="'.$aula['disciplina'].' - '.$aula['professor'].'">
									'.$aula['disciplina'].' - '.$aula['professor'].'
								  </span>';
						}
					
					} else {
						echo '<span class="event fds d-block p-1 pl-2 pr-2 mb-1 rounded text-truncate small bg-muted text-white" data-dia="'.$dia.'">
									Sem aula
							  </span>';
					}
				?>
			</div>
		<?php } ?>
		
    	<!-- julho -->
	    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-fim text-muted">
	      <h5 class="row align-items-center">
	        <span class="date col-1">1</span>
	      </h5>
	      <p class="d-sm-none">Sem Eventos</p>
	    </div>

	    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-fim text-muted">
	      <h5 class="row align-items-center">
	        <span class="date col-1">2</span>
	      </h5>
	      <p class="d-sm-none">Sem Eventos</p>
	    </div>

	    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-fim text-muted">
	      <h5 class="row align-items-center">
	        <span class="date col-1">3</span>
	      </h5>
	      <p class="d-sm-none">Sem Eventos</p>
	    </div>

	    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-fim text-muted">
	      <h5 class="row align-items-center">
	        <span class="date col-1">4</span>
	      </h5>
	      <p class="d-sm-none">Sem Eventos</p>
	    </div>

	    <div class="day col-sm p-2 border border-left-0 border-top-0 text-truncate d-none d-sm-inline-block bg-fim text-muted">
	      <h5 class="row align-items-center">
	        <span class="date col-1">5</span>
	      </h5>
	      <p class="d-sm-none">Sem Eventos</p>
	    </div>
		<!-- julho -->    

  	</div>
</div>    

<?php include_once './dist/template/rodape.php'; ?>
        